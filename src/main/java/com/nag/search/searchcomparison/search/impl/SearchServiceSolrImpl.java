package com.nag.search.searchcomparison.search.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nag.search.searchcomparison.model.SearchDocModel;
import com.nag.search.searchcomparison.model.SearchMatchModel;
import com.nag.search.searchcomparison.model.SearchResultModel;
import com.nag.search.searchcomparison.search.SearchService;
import com.nag.search.searchcomparison.solr.repo.SearchDocSolrModelCustomRepo;

@Service(SearchService.SOLR_SERVICE)
public class SearchServiceSolrImpl implements SearchService{

	private static Logger LOG = LoggerFactory.getLogger(SearchServiceSolrImpl.class);

	@Autowired
	private SearchDocSolrModelCustomRepo solrCuRepo;
	
	@Override
	public SearchResultModel search(String searchTearm) {
		
		SearchResultModel result = new SearchResultModel();
		QueryResponse response = solrCuRepo.search(searchTearm);
		
		SolrDocumentList results = response.getResults();
		
		List<SearchMatchModel> matches = results.stream()
				.map(s->new SearchMatchModel((String) s.get("title"), (Integer) s.get("matchCount")))
				.collect(Collectors.toList());
		
		result.setElapsedTimeSec(response.getQTime());
		result.setMatches(matches);

		return result;
	}

	
	
}
