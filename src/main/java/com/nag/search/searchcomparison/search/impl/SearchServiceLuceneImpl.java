package com.nag.search.searchcomparison.search.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nag.search.searchcomparison.SearchComparisonDataSetup;
import com.nag.search.searchcomparison.lucene.LuceneSearchIndex;
import com.nag.search.searchcomparison.model.SearchDocModel;
import com.nag.search.searchcomparison.model.SearchMatchModel;
import com.nag.search.searchcomparison.model.SearchResultModel;
import com.nag.search.searchcomparison.search.SearchService;

@Service(SearchService.LUCENE_SERVICE)
public class SearchServiceLuceneImpl implements SearchService {

	private static Logger LOG = LoggerFactory.getLogger(SearchServiceLuceneImpl.class);

	@Autowired
	private LuceneSearchIndex indexSearch;
	
	@Override
	public SearchResultModel search(String term) {
		
		SearchResultModel result = new SearchResultModel();
		
		long start = System.currentTimeMillis();
		List<SearchDocModel> searchResult = indexSearch.fuzzyTermSearch(term);
		long end = System.currentTimeMillis();
		
		List<SearchMatchModel> matches = searchResult.stream().map(s->new SearchMatchModel(s.getTitle(),1)).collect(Collectors.toList());
		result.setMatches(matches);
		result.setElapsedTimeSec(end-start);
		LOG.info("Matches for term {} : {}",term,result);
		return result;
	}

	
}
