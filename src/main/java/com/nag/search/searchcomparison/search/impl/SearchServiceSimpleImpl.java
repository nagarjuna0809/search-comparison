package com.nag.search.searchcomparison.search.impl;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nag.search.searchcomparison.dao.SearchDocDao;
import com.nag.search.searchcomparison.model.SearchMatchModel;
import com.nag.search.searchcomparison.model.SearchResultModel;
import com.nag.search.searchcomparison.search.SearchService;

@Service(SearchService.SIMPLE_SERVICE)
public class SearchServiceSimpleImpl implements SearchService{

	private static Logger LOG = LoggerFactory.getLogger(SearchServiceSimpleImpl.class);

	@Autowired
	private SearchDocDao searchDocDao;
	
	@Override
	public SearchResultModel search(String searchTearm) {
		
		SearchResultModel result = new SearchResultModel();
		
		long start = System.currentTimeMillis();
		List<SearchMatchModel> matches = simpleSearch(searchTearm);
		long end = System.currentTimeMillis();
		
		result.setElapsedTimeSec(end-start);
		result.setMatches(matches);

		return result;
	}
	
	public List<SearchMatchModel> simpleSearch(String term){
		
		List<SearchMatchModel> result = new ArrayList<>();
		
		searchDocDao.findAll().
		forEach(c->{
			result.add(
					new SearchMatchModel(c.getTitle(), 
					getTermFrequency(c.getText(),term))
					);
		});
		
		SearchService.sortByRelevance(result);
		
		return result;
		
	}

	public Integer getTermFrequency(String text, String term){
		String[] bagOfWords = text.split(" ");
		
		int termFreq=0;
		for (String word : bagOfWords) {
			if(word.equalsIgnoreCase(term)){
				termFreq++;
			}
		}
		
		return termFreq;
	}

	
	
}
