package com.nag.search.searchcomparison.search.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nag.search.searchcomparison.dao.SearchDocDao;
import com.nag.search.searchcomparison.model.SearchMatchModel;
import com.nag.search.searchcomparison.model.SearchResultModel;
import com.nag.search.searchcomparison.search.SearchService;

@Service(SearchService.REGEX_SERVICE)
public class SearchServiceRegexImpl implements SearchService{
	
	private static Logger LOG = LoggerFactory.getLogger(SearchServiceRegexImpl.class);

	@Autowired
	private SearchDocDao searchDocDao;
	
	@Override
	public SearchResultModel search(String searchTearm) {
		
		SearchResultModel result = new SearchResultModel();
		
		long start = System.currentTimeMillis();
		List<SearchMatchModel> matches = simpleSearch(searchTearm);
		long end = System.currentTimeMillis();
		
		result.setElapsedTimeSec(end-start);
		result.setMatches(matches);
		
		return result;
	}
	
	/**
	 * Searches the given term and calculates term frequency and then sorts based on relevance
	 * 
	 */
	public List<SearchMatchModel> simpleSearch(String term){
		
		List<SearchMatchModel> result = new ArrayList<>();
		String termRegex = "\\b"+term+"\\b";
		searchDocDao.findAll().
		forEach(c->{
			result.add(
					new SearchMatchModel(c.getTitle(), 
					getTermFrequency(c.getText(),termRegex))
					);
		});
		
		SearchService.sortByRelevance(result);
		
		return result;
		
	}
	
	/**
	 * matches the given term in each document and counts the term frequency per document
	 * 
	 * @param text
	 * @param term
	 * @return
	 */
	public Integer getTermFrequency(String text, String term){
		Pattern pattern = Pattern.compile(term, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(text);
        int termFreq=0;
        while (matcher.find())
        	termFreq++;
        
		return termFreq;
	}

	
	
}
