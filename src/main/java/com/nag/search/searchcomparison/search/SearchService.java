package com.nag.search.searchcomparison.search;

import java.util.Comparator;
import java.util.List;

import com.nag.search.searchcomparison.model.SearchMatchModel;
import com.nag.search.searchcomparison.model.SearchResultModel;
import com.nag.search.searchcomparison.search.impl.SearchServiceLuceneImpl;
import com.nag.search.searchcomparison.search.impl.SearchServiceRegexImpl;
import com.nag.search.searchcomparison.search.impl.SearchServiceSimpleImpl;
import com.nag.search.searchcomparison.search.impl.SearchServiceSolrImpl;
/**
 * 
 * @author Nagarjuna
 * 
 * <p>Common interface for all search implementations</p>
 * <p>Refer to the implementation class </p>
 * {@link SearchServiceSolrImpl} <br>
 * {@link SearchServiceSimpleImpl} <br>
 * {@link SearchServiceRegexImpl} <br>
 * {@link SearchServiceLuceneImpl} <br>
 *
 */
public interface SearchService {

	String SOLR_SERVICE ="solrSearchService";
	String LUCENE_SERVICE = "luceneSearchService";
	String REGEX_SERVICE = "regexSearchService";
	String SIMPLE_SERVICE = "simpleSearchService";
	
	/**
	 * 
	 * <p>Pass the term to generate the search results. This does the following functions.</p>
	 * 
	 * <ul>
	 * <li> Searches the given term</li> 
	 * <li>calculates term frequency </li>
	 * <li> sorts based on relevance </li>
	 * <li> calculates time taken to perform the search</li>
	 * </ul>
	 * 
	 * @param term
	 * @return
	 */
	SearchResultModel search(String term);
	
	
	/**
	 * 
	 * Sorts the given list of {@link SearchMatchModel} based on match count to make it relevant.
	 * 
	 * @param result
	 */
	public static void sortByRelevance(List<SearchMatchModel> result) {
		result.sort(Comparator.comparingInt(SearchMatchModel::getMatchCount).reversed());
	}
}
