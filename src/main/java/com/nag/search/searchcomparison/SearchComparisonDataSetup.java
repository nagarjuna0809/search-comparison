package com.nag.search.searchcomparison;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

import com.google.common.base.Charsets;
import com.google.common.io.Resources;
import com.nag.search.searchcomparison.dao.SearchDocDao;
import com.nag.search.searchcomparison.model.SearchDocModel;
import com.nag.search.searchcomparison.model.SearchDocSolrModel;
import com.nag.search.searchcomparison.search.SearchService;
import com.nag.search.searchcomparison.solr.repo.SearchDocSolrModelCustomRepo;
import com.nag.search.searchcomparison.solr.repo.SearchDocSolrModelRepository;

@Component
@DependsOn("searchDocDao")
public class SearchComparisonDataSetup {
	private static Logger LOG = LoggerFactory.getLogger(SearchComparisonDataSetup.class);

	@Autowired
	private SearchDocDao searchDocDao;

	@Autowired
	@Qualifier(SearchService.LUCENE_SERVICE)
	private SearchService luceneSearch;

	@Autowired
	private SearchDocSolrModelRepository solrRepo;

	private List<String> files = Arrays.asList("french_armed_forces.txt", "hitchhikers.txt", "warp_drive.txt");

	private void addDoc(String title, String text) {
		SearchDocModel doc = new SearchDocModel();
		doc.setTitle(title);
		doc.setText(text);

		searchDocDao.save(doc);
	}

	private void addSolrDoc(String title, String text) {
		SearchDocSolrModel doc = new SearchDocSolrModel(title, text);
		solrRepo.save(doc);
	}

	@PostConstruct
	public void addSearchDocs() {

		
		LOG.info("ADDING SEARCH DOCS");
		solrRepo.deleteAll();

		for (String fileName : files) {
			addSolrDoc(fileName, getFileText(fileName));
			addDoc(fileName, getFileText(fileName));
		}
		
	}

	private String getFileText(String fileName) {
		try {
			return Resources.toString(Resources.getResource("sample_text/" + fileName), Charsets.UTF_8);

		} catch (IOException e) {
			System.err.println(e);
			return null;
		}
	}

}
