package com.nag.search.searchcomparison.model;

import java.util.List;

public class SearchResultModel {
	
	private List<SearchMatchModel> matches;
	private long elapsedTimeSec;
	
	public List<SearchMatchModel> getMatches() {
		return matches;
	}
	public void setMatches(List<SearchMatchModel> matches) {
		this.matches = matches;
	}
	public long getElapsedTimeSec() {
		return elapsedTimeSec;
	}
	public void setElapsedTimeSec(long elapsedTimeSec) {
		this.elapsedTimeSec = elapsedTimeSec;
	}
	
	@Override
	public String toString() {
		return "SearchResultModel [matches=" + matches + ", elapsedTimeSec=" + elapsedTimeSec + "]";
	}
	

}
