package com.nag.search.searchcomparison.model;

public class SearchMatchModel {
	
	private String title;
	private Integer matchCount;
	
	public SearchMatchModel(String title, Integer matchCount) {
		super();
		this.title = title;
		this.matchCount = matchCount;
	}
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public Integer getMatchCount() {
		return matchCount;
	}
	public void setMatchCount(Integer matchCount) {
		this.matchCount = matchCount;
	}

	
	@Override
	public String toString() {
		return "SearchMatchModel [title=" + title + ", matchCount=" + matchCount + "]";
	}

}
