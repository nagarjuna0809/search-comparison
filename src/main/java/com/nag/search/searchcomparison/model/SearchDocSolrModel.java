package com.nag.search.searchcomparison.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.solr.core.mapping.Indexed;
import org.springframework.data.solr.core.mapping.SolrDocument;

import com.nag.search.searchcomparison.solr.SolrConfiguration;

@SolrDocument(solrCoreName = SolrConfiguration.SOLR_CORE_NAME)
public class SearchDocSolrModel {

	@Id
	@Indexed(name = "id", type = "string")
	
	private String id;

	@Indexed(name = "text", type = "string")
	private String text;

	public SearchDocSolrModel() {
	}
	
	public SearchDocSolrModel(String id, String text) {
		super();
		this.id = id;
		this.text = text;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	@Override
	public String toString() {
		return "SearchDocSolrModel [id=" + id + ", text=" + text + "]";
	}

}
