package com.nag.search.searchcomparison;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SearchComparisonApplication {

	public static void main(String[] args) {
		SpringApplication.run(SearchComparisonApplication.class, args);
	}


}
