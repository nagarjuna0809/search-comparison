package com.nag.search.searchcomparison.lucene;

import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.transaction.Transactional;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.MultiFields;
import org.apache.lucene.index.PostingsEnum;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.highlight.Fragmenter;
import org.apache.lucene.search.highlight.Highlighter;
import org.apache.lucene.search.highlight.QueryScorer;
import org.apache.lucene.search.highlight.SimpleSpanFragmenter;
import org.apache.lucene.search.spans.SpanTermQuery;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.hibernate.search.jpa.FullTextEntityManager;
import org.hibernate.search.jpa.Search;
import org.hibernate.search.query.dsl.QueryBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.nag.search.searchcomparison.model.SearchDocModel;

@Component
public class LuceneSearchIndex {

	private static Logger LOG = LoggerFactory.getLogger(LuceneSearchIndex.class);

	@Autowired
    private EntityManager entityManager;
	
	private FullTextEntityManager fullTextEntityManager;
	private QueryBuilder qb;
	private IndexSearcher indexSearcher;
	
	//analyzer with the default stop words
	private Analyzer analyzer = new StandardAnalyzer();
	
	@PostConstruct
	public void searchInit(){
		
	}
	
	public List<SearchDocModel> fuzzyTermSearch(String searchTerm) {
		fullTextEntityManager = Search.getFullTextEntityManager(entityManager);
    	qb = fullTextEntityManager.getSearchFactory().buildQueryBuilder().forEntity(SearchDocModel.class).get();
    	
        SpanTermQuery luceneQuery = new SpanTermQuery(new Term("text", searchTerm));

		QueryScorer queryScorer = new QueryScorer(luceneQuery, "text");
        Fragmenter fragmenter = new SimpleSpanFragmenter(queryScorer);

        Highlighter highlighter = new Highlighter(queryScorer); // Set the best scorer fragments
        highlighter.setTextFragmenter(fragmenter); // Set fragment to highlight 
        
      //Get directory reference
        Directory dir;
		try {
			dir = FSDirectory.open(Paths.get("indexes/com.nag.search.searchcomparison.model.SearchDocModel"));
		
         
        //Index reader - an interface for accessing a point-in-time view of a lucene index
        IndexReader indexReader = DirectoryReader.open(dir);
        
//        IndexReader indexReader = fullTextEntityManager.getSearchFactory().getIndexReaderAccessor().open(SearchDocModel.class);        
        indexSearcher = new IndexSearcher(indexReader);
        PostingsEnum docEnum = MultiFields.getTermDocsEnum(indexReader, "text",new Term("text", searchTerm).bytes());
        int doc = PostingsEnum.NO_MORE_DOCS;
        while ((doc = docEnum.nextDoc()) != PostingsEnum.NO_MORE_DOCS) {
        	LOG.info("Term Frequency for {}",docEnum.freq());
        }
        
		} catch (Exception e) {
			LOG.error("Unknown error ",e);
		}
        return Collections.emptyList();
	}
	
    @SuppressWarnings("unchecked")
	@Transactional
    public List<SearchDocModel> fuzzySearch(String searchTerm) {
    	fullTextEntityManager = Search.getFullTextEntityManager(entityManager);
    	qb = fullTextEntityManager.getSearchFactory().buildQueryBuilder().forEntity(SearchDocModel.class).get();
    	
        SpanTermQuery luceneQuery = new SpanTermQuery(new Term("text", searchTerm));
                   
        javax.persistence.Query jpaQuery = fullTextEntityManager.createFullTextQuery(luceneQuery, SearchDocModel.class);
        
        List<SearchDocModel> searchDocModelList = null;
        try {
        	
        	searchDocModelList = jpaQuery.getResultList();
        	
        } catch (NoResultException nre) {
        		LOG.error("NO RESULT",nre);
        }

        return searchDocModelList;
    }

}
