package com.nag.search.searchcomparison.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.nag.search.searchcomparison.model.SearchResultModel;
import com.nag.search.searchcomparison.search.SearchService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * <p>
 * Pass the term to generate the search results. This does the following
 * functions.
 * </p>
 * 
 * <ul>
 * <li>Searches the given term</li>
 * <li>calculates term frequency</li>
 * <li>sorts based on relevance</li>
 * <li>calculates time taken to perform the search</li>
 * </ul>
 * 
 * @author Nagarjuna
 *
 */
@Api(value="onlinestore", description="Pass the term to generate the search results. This does the following"
 +" functions. Searches the given term, calculates term frequency, sorts based on relevance and then calculates time taken to perform the search.")
@RestController
public class SearchComparisionController {

	@Autowired
	@Qualifier(SearchService.REGEX_SERVICE)
	private SearchService regexSearchService;

	@Autowired
	@Qualifier(SearchService.SIMPLE_SERVICE)
	private SearchService simpleSearchService;

	@Autowired
	@Qualifier(SearchService.SOLR_SERVICE)
	private SearchService solrSearchService;

	@Autowired
	@Qualifier(SearchService.LUCENE_SERVICE)
	private SearchService luceneSearchService;

	@ApiOperation(value="Pass the term to generate the search results, it does Simple search",response=SearchResultModel.class)
	@GetMapping("/simple")
	public SearchResultModel simpleSearchService(@RequestParam("term") String term) {

		SearchResultModel result = simpleSearchService.search(term);

		return result;
	}

	@ApiOperation(value="Pass the term to generate the search results, it does Regex search",response=SearchResultModel.class)
	@GetMapping("/regex")
	public SearchResultModel regexSearchService(@RequestParam("term") String term) {

		SearchResultModel result = regexSearchService.search(term);

		return result;
	}

	@ApiOperation(value="Pass the term to generate the search results, it does Solr search",response=SearchResultModel.class)
	@GetMapping("/solrIndex")
	public SearchResultModel solrSearchService(@RequestParam("term") String term) {

		SearchResultModel result = solrSearchService.search(term);

		return result;
	}
}
