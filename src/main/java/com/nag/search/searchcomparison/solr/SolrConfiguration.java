package com.nag.search.searchcomparison.solr;

import org.apache.solr.client.solrj.SolrClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.solr.core.SolrTemplate;
import org.springframework.data.solr.repository.config.EnableSolrRepositories;

@Configuration
@EnableSolrRepositories(multicoreSupport = true)
public class SolrConfiguration {

	public static final String SOLR_CORE_NAME="searchcomparison";
	
    @Bean
    public SolrTemplate solrTemplate(@Autowired SolrClient client) throws Exception {
        return new SolrTemplate(client);
    }
}
