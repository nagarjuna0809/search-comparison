package com.nag.search.searchcomparison.solr.repo;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.solr.core.query.result.HighlightPage;
import org.springframework.data.solr.repository.Highlight;
import org.springframework.data.solr.repository.SolrCrudRepository;

import com.nag.search.searchcomparison.model.SearchDocSolrModel;

public interface SearchDocSolrModelRepository extends SolrCrudRepository<SearchDocSolrModel, String> {
	List<SearchDocSolrModel> findByTextEndsWith(String text);
	
	@Highlight(prefix = "<highlight>", postfix = "</highlight>")
	HighlightPage<SearchDocSolrModel> findByText(String description, Pageable pageable);
	
	
}
