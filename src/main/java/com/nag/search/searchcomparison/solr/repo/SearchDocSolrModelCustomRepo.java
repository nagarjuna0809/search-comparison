package com.nag.search.searchcomparison.solr.repo;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.solr.core.SolrTemplate;
import org.springframework.stereotype.Component;

import com.nag.search.searchcomparison.model.SearchMatchModel;
import com.nag.search.searchcomparison.solr.SolrConfiguration;

import org.slf4j.Logger;

@Component
public class SearchDocSolrModelCustomRepo {

	private static Logger LOG = LoggerFactory.getLogger(SearchDocSolrModelCustomRepo.class);
	@Autowired
	private SolrTemplate solrTemplate;
	
	@Autowired
	private SolrClient solrClient;

	public QueryResponse search(String searchTearm){
		SolrQuery query = new SolrQuery();
        query.setQuery("*:*");
        query.set("fl", "title:id,matchCount:termfreq(\"text\",\""+searchTearm+"\")");
        query.set("sort", "termfreq(\"text\",\""+searchTearm+"\") desc");
        QueryResponse response=null;
		try {
			response = solrClient.query(SolrConfiguration.SOLR_CORE_NAME,query);
			
		} catch (SolrServerException | IOException e) {
			LOG.error("",e);
		}
		return response;
	}
	
}
