package com.nag.search.searchcomparison.dao;


import org.springframework.data.repository.CrudRepository;

import com.nag.search.searchcomparison.model.SearchDocModel;


public interface SearchDocDao extends CrudRepository<SearchDocModel,Long> {

}