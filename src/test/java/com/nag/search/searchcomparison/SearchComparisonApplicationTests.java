package com.nag.search.searchcomparison;

import static org.junit.Assert.assertNotNull;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.carrotsearch.junitbenchmarks.AbstractBenchmark;
import com.carrotsearch.junitbenchmarks.BenchmarkOptions;
import com.nag.search.searchcomparison.model.SearchResultModel;
import com.nag.search.searchcomparison.search.SearchService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SearchComparisonApplicationTests  extends AbstractBenchmark{

	@Autowired
	@Qualifier(SearchService.REGEX_SERVICE)
	private SearchService regexSearchService; 
	
	@Autowired
	@Qualifier(SearchService.SIMPLE_SERVICE)
	private SearchService simpleSearchService; 
	
	@Autowired
	@Qualifier(SearchService.SOLR_SERVICE)
	private SearchService solrSearchService; 
	
	@Autowired
	@Qualifier(SearchService.LUCENE_SERVICE)
	private SearchService luceneSearchService; 
	
	private List<String> searchTerms= Arrays.asList("the","than","used","hello","from","tool","export","bean","process","scanner","boot","handle");
	
	private Random rand=new Random();
	
	@Test
	@BenchmarkOptions(benchmarkRounds = 100, warmupRounds = 5)
	public void simpleSearchServiceTest() {
		String generatedString = RandomStringUtils.random(5, true, false);

		SearchResultModel result = simpleSearchService.search(generatedString);
		
		 assertNotNull(result);
	}
	
	@Test
	@BenchmarkOptions(benchmarkRounds = 100, warmupRounds = 5)
	public void regexSearchServiceTest() {
		String generatedString = RandomStringUtils.random(5, true, false);

		SearchResultModel result = regexSearchService.search(generatedString);
		
		assertNotNull(result);
	}
	
	@Test
	@BenchmarkOptions(benchmarkRounds = 100, warmupRounds = 5)
	public void solrSearchServiceTest() {
		String generatedString = RandomStringUtils.random(5, true, false);

		SearchResultModel result = solrSearchService.search(generatedString);
		
		assertNotNull(result);
	}

}
