## Search Comparision Project

## Getting Started
The goal of this exercise is to create a working program to search a set of documents for the given search term or phrase (single token), and return results in order of relevance. 
Relevancy is defined as number of times the exact term or phrase appears in the document. 

## Technologies
* Java 8
* Spring Boot 1.5.9
* Hibernate 
* Lucene 5.5.4
* Solr 
* HSQL

## Dependencies / Prerequisites 
* Solr Server Hosted in http://35.224.53.118:8983/solr/#/
* UI Application http://35.224.53.118:3006/

####Create three methods for searching the documents: 

*	Simple string matching
*	Text search using regular expressions
*	Preprocess the content and then search the index

### Steps to run the project
  Pull the project to local and run the below command project path 
```
 ./mvnw spring-boot:run
```

#### Testing
Use the Swagger UI to test the results.
```
http://localhost:8080/swagger-ui.html#/search-comparision-controller
```